<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BIP Auto Peças e Distribuidora | Vans, Pick-ups e Utilitários</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="assets/css/style-alt.css" rel="stylesheet">    
</head>
<body>

  <!-- MENU PRINCIPAL -->
  <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light col-12 col-md-9">
                <a class="navbar-brand" href="#"><img class="img-topo" src="../assets/images/logo-bip.png"></a>
                    <div class="menu-urls collapse navbar-collapse">
                      <ul class="navbar">
                          <li class="nav-item">
                              <a href="/" class="nav-link" title="Saiba mais sobre a BIP Auto Peças e Distribuidora">Home</a>
                            </li>
                        <li class="nav-item">
                          <a href="/quem-somos" class="nav-link" title="Saiba mais sobre a BIP Auto Peças e Distribuidora">Quem Somos</a>
                        </li>
                        <li class="nav-item">
                          <a href="/produtos" class="nav-link" title="Conheça os tipos de peças e segmentos que a BIP Auto Peças trabalha">Produtos</a>
                        </li>
            
                        <li class="nav-item">
                          <a href="/localizacao" class="nav-link" title="Conheça a localização da BIP Auto Peças">Localização</a>
                        </li>
            
                        <li class="nav-item">
                          <a href="/contato" class="nav-link" title="Entre em contato com a BIP Auto Peças">Contato</a>
                        </li>
            
                        <li class="nav-item orcamento text-center">
                            <a href="..." class="nav-link btn-yellow">Orçamentos</a>
                        </li>
                      </ul>
                    </div>
              </nav>
              <img class="background-img" src="../assets/images/top-background.png" alt="...">
              <div class="hamburguer-menu d-lg-none d-sm-block">
    <div class="dropdown show">
  <a class="btn-lg dropdown-toggle" width="300" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <img src="assets/images/icone-hamburguer.png" width="100">
  </a>

  <div class="dropdown-menu btn-lg" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item h2" href="/">Home</a>
    <a class="dropdown-item h2" href="/quem-somos">Quem Somos</a>
    <a class="dropdown-item h2" href="/produtos">Produtos</a>
    <a class="dropdown-item h2" href="/localizacao">Localização</a>
    <a class="dropdown-item h2" href="/contato">Contato</a>
    <a class="dropdown-item h2" href="/orcamento">Orçamentos</a>
  </div>
</div>
    </div>
  </div>
<br><br><br><br><br><br>
<!-- MENU PRINCIPAL -->
<div class="container text-center">
<?php

echo "Você está em: Home : Quem Somos";
?>

<!-- SOBRE NÓS -->
<div class="container col-12 col-md-5 col-lg-4">
                    <p class="h2 mt-5 mb-3 text-left">Sobre Nós</p>
                    <p class="about-text">Há mais de 10 anos no mercado, localizada na cidade de Jundiaí, interior de São Paulo, a B.I.P. – Importação Comércio e Serviços LTDA - surgiu com o intuito de oferecer a solução correta aos nossos consumidores, fornecendo ao mercado de autopeças as melhores marcas com produtos originais com garantia e disponibilidade de estoque para pronta entrega.</p>

                    <p class="about-text">A B.I.P. conta com uma equipe de profissionais especializados. Somos referência em qualidade de serviços, atendendo condutores de transporte escolar, frotistas, transportadoras, empresas de transporte executivo, oficinas mecânicas, lojas de
                    autopeças e cooperativas.</p>

                    <p class="about-text">A empresa se concentra na atividade de comercialização de autopeças para Vans, Pick-ups e Utilitários e tem se destacado para atender um mercado especializado, fornecendo peças originais e de reposição para Fiat Ducato, Renault Master, Hyundai HR, Kia K2500, Mercedes Sprinter, Mitsubishi L200, Toyota Hilux, Besta, Topic, EFFA, Haffei, entre outros
                    veículos.</p>
                    <div class="container">
                        <div class="sobre-nos">
                    <img  class="img-2" src="../assets/images/sobre-nos-img2.png" alt="...">
                  </div>
                  <div class="container mb-5">
                  <p class="h2 mt-5 mb-3">Estrutura</p>
                  <img class="" src="../assets/images/structure.png" alt="Nossa Estrutura">
                  </div>
                </div>
                  </div>
<!-- SOBRE NÓS -->

</div>
<!--FOOTER -->
                <footer>
                    <div class="text-sm-right barra-azul">
                        &nbsp;
                      </div>

          <!-- ENTRE EM CONTATO -->
                        <div class="container">

                          <div class="row">
                                <div class="col-sm text-white bg-dark text-center">
                                <p class="h5 text-center pt-3">Entre em Contato</p>
                                  <button type="button" class="btn btn-success"><img src="../assets/images/telephone-icon.png" alt="">(11)4587-3987</button>
                                  <p class="h6 pb-2 pt-3"><img src="../assets/images/whatsap-sm-icon.png" alt="">(11) 99940-9898<img src="images/e-mail.png" alt="">bip@bippecas.com.br</p>
                                </div>

                                <div class="col-sm text-white bg-primary">
                                <p class="h5 text-center pt-3">Horário de funcionamento</p>
                                  <p class="h6 text-center"><img src="../assets/images/clock-icon.png" alt="...">de Segunda à sexta das 08:00 às 18:00,
                                      e aos sábados das 08:00 às 12:00
                                      <p class="text-warning text-center pb-4">(sem pausa para almoço)</p></p>
                                    </div>

                                <div class="col-sm text-white bg-dark">
                                    <p class="h5 text-center pt-3">Nossa Localização</p>
                                      <p class="h6 text-center pb-2"><img src="../assets/images/map-marker-icon.png" alt="...">Rua Tibiriçá, 160, Vila Arens II<br> Jundiaí/SP</p>
                                    </div>
                            </div>
                          </div>

            <!-- ENTRE EM CONTATO-->
                    <div class="row justify-content-center">
                    <ul class="menu-footer">
                      <img class="mr-5" src="../assets/images/logo-bip2.png" alt="">
                      <li class="d-inline mr-3"><a class="footer-links" href="/">Home</a></li>
                      <li class="d-inline mr-3"><a class="footer-links" href="/quem-somos">Quem Somos</a></li>
                      <li class="d-inline mr-3"><a class="footer-links" href="/produtos">Produtos</a></li>
                      <li class="d-inline mr-3"><a class="footer-links" href="/localizacao">Localização</a></li>
                      <li class="d-inline mr-3"><a class="footer-links" href="/contato">Contato</a></li>
                      <li class="d-inline mr-3"><a class="footer-links" href="/orcamentos">Orçamentos</a></li>
                    </ul>
                  </div>
                </footer>

<!-- FOOTER -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous" async></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"async></script>
</body>
</html>